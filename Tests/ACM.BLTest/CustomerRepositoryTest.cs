﻿using ACM.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ACM.BLTest
{
    [TestClass]
    public class CustomerRepositoryTest
    {
        [TestMethod]
        public void RetrieveValid()
        {
            var customerRepository = new CustomerRepository();
            var expexted = new Customer(1)
            {
                EmailAddress = "fbaggins@hobbiton.me",
                FirstName = "Frodo",
                LastName = "Baggins"
            };
            var actual = customerRepository.Retrieve(1);
            Assert.AreEqual(expexted.CustomerId, actual.CustomerId);
            Assert.AreEqual(expexted.EmailAddress, actual.EmailAddress);
            Assert.AreEqual(expexted.FirstName, actual.FirstName);
            Assert.AreEqual(expexted.LastName, actual.LastName);
        }
        [TestMethod]
        public void RetrieveExistingWithAddress() 
        {
            var customerRepositoty = new CustomerRepository();
            var expexted = new Customer(1)
            {
                EmailAddress = "fbaggins@hobbiton.me",
                FirstName = "Frodo",
                LastName = "Baggins",
                AddressList = new List<Address>()
                {
                    new Address()
                    {
                        AddressType = 1,
                        StreetLine1 = "Bag End",
                        StreetLine2 = "Bagshot row",
                        City = "Hobbiton",
                        State = "Shire",
                        Country = "Middle Earth",
                        PostalCode = "144"
                    },
                    new Address()
                    {
                        AddressType = 2,
                        StreetLine1 = "Gree Dragon",
                        City = "Bywater",
                        State = "Shire",
                        Country = "Middle Earth",
                        PostalCode = "146"
                    }

                }
                        
            };

            var actual = customerRepositoty.Retrieve(1);
            Assert.AreEqual(expexted.CustomerId, actual.CustomerId);
            Assert.AreEqual(expexted.EmailAddress, actual.EmailAddress);
            Assert.AreEqual(expexted.FirstName, actual.FirstName);
            Assert.AreEqual(expexted.LastName, actual.LastName);

            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(expexted.AddressList[i].AddressType, actual.AddressList[i].AddressType);
                Assert.AreEqual(expexted.AddressList[i].StreetLine1, actual.AddressList[i].StreetLine1);
                Assert.AreEqual(expexted.AddressList[i].City, actual.AddressList[i].City);
                Assert.AreEqual(expexted.AddressList[i].State, actual.AddressList[i].State);
                Assert.AreEqual(expexted.AddressList[i].Country, actual.AddressList[i].Country);
                Assert.AreEqual(expexted.AddressList[i].PostalCode, actual.AddressList[i].PostalCode);
            }
        }
    }
}
